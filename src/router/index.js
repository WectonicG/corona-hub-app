import { createRouter, createWebHistory } from '@ionic/vue-router';

const routes = [
  {
    path: '/',
    component: () => import ('../views/Index.vue')
  },
  {
    path: '/states',
    component: () => import ('../views/States.vue')
  },
  {
    path: '/districts',
    component: () => import ('../views/Districts.vue')
  },
  {
    path: '/vaccinations',
    component: () => import ('../views/Vaccinations.vue')
  },
  {
    path: '/sources',
    component: () => import ('../views/Sources.vue')
  },
  {
    path: '/info',
    component: () => import ('../views/Info.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
